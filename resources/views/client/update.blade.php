@extends('layout.master')

@section('content')

  <!-- Main content -->
  <br>
    <div class="container">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            @empty ($profiles->image)
                            <img class="profile-user-img img-fluid img-circle" src="{{asset('/images/profile.png')}}" alt="User profile picture">
                            @endempty
                            @isset ($profiles->image)
                            <img class="profile-user-img img-fluid img-circle" src="{{asset('images/img_profile/'.$profiles->image)}}" alt="User profile picture">
                            @endisset
                        </div>
                        {{-- @foreach ($profiles->users->id as $profile) --}}
                        <h3 class="profile-username text-center">{{$profiles->user->name}}</h3>
                        {{-- @endforeach --}}

                        {{-- <p class="text-muted text-center">Software Engineer</p> --}}
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- About Me Box -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">About Me</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <strong><i class="fa fa-envelope-o" aria-hidden="true"></i> Email</strong>

                        <p class="text-muted">
                            {{$profiles->user->email}}
                        </p>

                        <hr>

                        <strong>Age</strong>

                        <p class="text-muted">{{$profiles->age}}</p>

                        <hr>

                        <strong><i class="fa fa-map-marker" aria-hidden="true"></i> Address</strong>

                        <p class="text-muted">{{$profiles->address}}</p>


                        <hr>

                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->






            <div class="col-md-9">






                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills">
                            <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li>
                        </ul>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane" id="settings">
                                <form action="/profile/{{$profiles->id}}" method="POST" class="form-horizontal " enctype="multipart/form-data">
                                    @method('put')
                                    @csrf
                                    <div class="form-group row">
                                        <label for="inputName" class="col-sm-2 col-form-label">Name</label>
                                        <div class="col-sm-10">
                                            <input type="name" name="name" class="form-control" value="{{$profiles->user->name}}" id="inputName" placeholder="Name">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputEmail" class="col-sm-2 col-form-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name="email" class="form-control" value="{{$profiles->user->email}}" id="inputEmail" placeholder="Email">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="image" class="col-sm-2 col-form-label">Foto Profile</label>
                                        <div class="col-sm-10">
                                            <input type="file" class="form-control" id="image" name="image">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputAge" class="col-sm-2 col-form-label">Age</label>
                                        <div class="col-sm-10">
                                            <input type="number" name="age" class="form-control" value="{{$profiles->age}}" id="inputAge" placeholder="Age">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="inputAddress" class="col-sm-2 col-form-label">Address</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="address" id="inputAddress" placeholder="Address">{{$profiles->address}}</textarea>
                                        </div>
                                    </div>
                                            <button type="submit" class="btn btn-danger">Update</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
<!-- /.content -->

@endsection
