@extends('layout.master')

@section('title')
    - Profile
@endsection

@section('content')
    <div class="container p-1">
        <div class="row mt-5 justify-content-center">
            <div class="col-2 align-self-center">
                @isset($record)   
                <img src="{{asset('images/img_profile/'. Auth::user()->profile->image)}}" class="rounded-circle" width="100px" alt="...">            
                @endisset
                @empty($record)
                <img src="{{asset('images/placeholder.png/')}}" class="rounded-circle" width="100px" alt="...">  
                @endempty
            </div>
            <div class="col-8 align-self-center">
                @isset($record)
                <h1 class="font-weight-bolder">{{Auth::user()->profile->name}}</h1>                    
                @endisset
                @empty($record)
                <h1 class="font-weight-bolder">{{Auth::user()->name}}</h1>  
                @endempty
                <p>{{Auth::user()->email}}</p>
            </div>
            <div class="col-2 align-self-center">
                <button class="btn btn-primary btn-lg pl-5 pr-5" data-toggle="modal" data-target="#modalProfile">Edit Profile</button>
            </div>
        </div>
        <div class="col mt-5">
            <div class="row question">
                <h2 class="font-weight-bold">Forum</h2>
            </div>
            <div class="col justify-content-center">
                @forelse ($questions as $question)
                        <a href="/question/{{$question->id}}" class="text-decoration-none text-dark">
                            <div class="card mb-3 border-0 p-2">
                                <div class="row no-gutters">
                                    <div class="col-md-2 mr-n5">
                                        @isset($record)
                                        <img src="{{asset('images/img_profile/'. Auth::user()->profile->image)}}" width="70px" class="rounded-circle img-thumbnail" alt="profile">
                                        @endisset
                                        @empty($record)
                                        <img src="{{asset('images/placeholder.png')}}" width="70px" class="rounded-circle img-thumbnail" alt="profile">
                                        @endempty
                                    </div>
                                    <div class="col-md-10 ml-n5">
                                        <div class="card-body">
                                            <h2 class="card-title font-weight-bold mb-n1">{{$question->title}} <span class="badge badge-primary" style="font-size: 10px">
                                                @foreach ($categories as $category)
                                                    @if ($category->id === $question->category_id)
                                                        {{$category->name}}
                                                    @endif
                                                @endforeach</span></h2>
                                            <p class="card-text"><small class="text-muted">Posted By {{$question->users->name}} - {{$question->users->created_at}}</small></p>
                                            <img src="{{asset('images/questions/'.$question->image)}}" class="image-wrapper float-left mr-3" width="100px" alt="...">
                                            <p class="card-text">{{Str::limit($question->question, 175)}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @empty
                        <div class="alert alert-info text-center mt-5">You haven't created any forums yet</div>
                        @endforelse
            </div>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="modalProfile" tabindex="-1" aria-labelledby="modalProfileLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalProfileLabel">Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="/profile/{{Auth::id()}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="name" name="name"
                                @isset($record)
                                value=
                                "
                                {{Auth::user()->profile->name}}
                                "
                                @endisset
                                @empty($record)
                                {{Auth::user()->name}}
                                @endempty
                                placeholder="Name">
                                @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="email" name="email" value="{{Auth::user()->email}}" placeholder="Email">
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                            </div>
                            <div class="form-group">
                                <input type="file" class="form-control" id="image" name="image">
                                @error('image')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="age" name="age" 
                                @isset($record)
                                value=
                                "
                                {{Auth::user()->profile->age}}
                                "
                                @endisset
                                @empty($record)                                            
                                @endempty
                                placeholder="Your age">
                                @error('age')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="address" name="address" 
                                @isset($record)
                                value=
                                "
                                {{Auth::user()->profile->address}}
                                "
                                @endisset
                                @empty($record)                                            
                                @endempty
                                placeholder="Address">
                                @error('address')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-3">
                                <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{Auth::user()->id}}">
                            </div>
                        </div>
                        <div class="card-footer bg-transparent border-top-0">
                            <button type="submit" class="btn btn-primary">Edit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection