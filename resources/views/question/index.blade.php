@extends('layout.master')

@section('content')
    <div class="gradient">
                <div id="intro" class="d-flex align-items-center justify-content-center">
                    <h4 class="text-white">Welcome to Stacker</h4>
                </div>
            </div>

            {{-- Sidebar Category --}}
            <div class="mt-5 d-flex justify-content-center">
                <div class="col-sm-3 d-flex justify-content-center">
                    <div class="sidebar">
                        <div class="input-group">
                            @if (Auth::id())
                                <button type="button" class="btn btn-outline-primary btn-sm pl-3 pr-3 rounded-pill font-weight-bolder" data-toggle="modal" data-target="#modalForum"><i class="fa-solid fa-circle-plus"></i>
                                    New Forum
                                </button>
                                @endif

                        </div>
                        <div class="all-forum mt-4 mb-4">
                            <a href="{{route('question.index')}}" class="text-decoration-none"><i class="fa-regular fa-comment"></i> All Forum</a>
                        </div>
                        <div class="form-inline">
                            @if (Auth::id())
                            <form method="POST" action="/category">
                                @csrf
                                <div class="form-group row">
                                    <div class="input-group">
                                        <button type="submit" class="btn btn-outline-primary btn-sm font-weight-bolder"><i class="fa-solid fa-pen"></i>
                                        </button>
                                        <input type="text" class="form-control" name="name" value="{{ old('name', '') }}" autocomplete="off" placeholder="Add Category">
                                    </div>
                                    @error('name')
                                        <small class="text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </form>
                            @endif
                        </div>
                        <div class="categories mt-4 mb-4" style="height: 200px; overflow-y: auto;">
                            @forelse ($categories as $category)
                            {{-- <a href="#" class="text-decoration-none d-block mb-3"><i class="fa-solid fa-square"></i> {{$category->name}}</a> --}}
                            <a href="/category/{{$category->name}}" class="text-decoration-none d-block mb-3"><i class="fa-solid fa-square"></i> {{$category->name}}</a>
                            @empty
                                No categories yet
                            @endforelse
                        </div>
                    </div>
                </div>

                {{-- content forum --}}
                <div class="col-sm-6 d-flex justify-content-center">
                    <div class="content-wrapper">
                        @if (session('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{session('success')}}</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                        <form class="form-inline mb-5 justify-content-center" action="/question">
                            <input class="form-control bg-transparent w-75" name="search" type="search" placeholder="Let's find a solution for your problem..." aria-label="Search">
                            <button class="btn btn-success" type="submit">Search</button>
                        </form>
                        @forelse ($questions as $question)
                        <a href="/question/{{$question->id}}" class="text-decoration-none text-dark">
                            <div class="card mb-3 border-0">
                                <div class="row no-gutters">
                                    <div class="col-md-2">
                                        @empty ($records)
                                        <img src="{{asset('images/dummy.jpg')}}" class="rounded-circle img-thumbnail" alt="profile">
                                        @endempty
                                        @isset ($records)
                                        <img src="{{asset('images/img_profile/'. Auth::user()->profile->image)}}"" class="rounded-circle img-thumbnail" alt="profile">
                                        @endisset
                                    </div>
                                    <div class="col-md-10">
                                        <div class="card-body">
                                            <h2 class="card-title font-weight-bold mb-n1">{{$question->title}} <span class="badge badge-primary" style="font-size: 10px">
                                                @foreach ($categories as $category)
                                                    @if ($category->id === $question->category_id)
                                                        {{$category->name}}
                                                    @endif
                                                @endforeach</span></h2>
                                            <p class="card-text"><small class="text-muted">Posted By {{$question->users->name}} - {{$question->users->created_at}}</small></p>
                                            {{-- <h4 class="card-title font-weight-bold mb-n1">{{$question->title}} --}}
                                                {{-- <span class="badge badge-primary" style="font-size: 10px">
                                                    {{$question->category->name}}
                                                </span>
                                            </h4>
                                            <p class="card-text">
                                                <small class="text-muted">
                                                    Posted By {{$question->users->name}}
                                                </small>
                                            </p> --}}
                                            <img src="{{asset('images/questions/'.$question->image)}}" class="image-wrapper float-left mr-3" width="100px" alt="...">
                                            <p class="card-text">{{Str::limit($question->question, 175)}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        @empty

                        @endforelse
                        <div>
                            Showing
                            {{$questions->firstItem()}}
                            to
                            {{$questions->lastItem()}}
                            of
                            {{$questions->total()}}
                            entries
                        </div>
                        {{ $questions->links() }}
                    </div>
                </div>

                {{-- Member Online --}}
                <div class="col-sm-2 d-flex justify-content-center">
                        <div class="sidebar">
                            <div class="new-forum">
                                <a href="#" class="btn btn-outline-primary pl-3 pr-3 rounded-pill font-weight-bolder"><i class="fa-solid fa-circle"></i> Online</a>
                                <ul class="list-unstyled mb-0 ">
                                    @if(count($users_online) > 0)
                                        @foreach ($users_online as $user)
                                        <li><a href="# ">{{$user->name}}</a></li><span class="badge badge-success">Online</span>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>


                @section('modal')
                    <div class="modal fade" id="modalForum">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalForumLabel">New Forum</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="/question" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body mb-n3">
                                        <div class="input-group mb-3 mt-n3">
                                            <input type="text" class="form-control" id="title" name="title" value="{{ old('title', '') }}" placeholder="Title">
                                            @error('title')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <select name="category_id" class="form-control">
                                                <option value="">--Select Category--</option>
                                                @foreach ($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" id="question" name="question" cols="30" rows="10" placeholder="Type your question here...">{{ old('question', '') }}</textarea>
                                            @error('question')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="file" class="form-control" id="image" name="image">
                                            @error('image')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="card-footer bg-transparent">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                            </form>
                            </div>
                        </div>
                    </div>
                @endsection
@endsection

