@extends('layout.master')

@section('title')
    - Create
@endsection

@section('content')
    <div class="container">
        <form action="/question" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">
                                        <input type="text" class="form-control mb-3" id="title" name="title" value="{{ old('title', '') }}" placeholder="Title">
                                            @error('title')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        <div class="input-group">                                            
                                            <select name="category_id" class="form-control">
                                                <option value="">--Select Category--</option>
                                                <option value="category_id">Programming</option>
                                                <option value="category_id">Programming</option>
                                                <option value="category_id">Programming</option>
                                            </select>
                                            @error('category_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <button type="button" class="btn btn-outline-primary pl-3 pr-3 font-weight-bolder" data-toggle="modal" data-target="#modalCategory" data-dismiss="modal"><i class="fa-solid fa-circle-plus"></i>
                                            </button>
                                        </div>
                                        <small class="form-text text-muted mb-3">If there is no category available in the select category form, add a category by pressing the button next to it</small>
                                        <div class="form-group">
                                            <textarea class="form-control" id="question" name="question" cols="30" rows="10" placeholder="Type your question here...">{{ old('question', '') }}</textarea>
                                            @error('question')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="file" class="form-control" id="image" name="image">
                                            @error('image')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        {{-- <div class="form-group">
                                            <label>Category</label>
                                            <select name="category_id" class="form-control">
                                                <option value="">--Pilih Category--</option>
                                                @foreach ($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div> --}}
                                    </div>
                                    <div class="card-footer bg-transparent border-top-0">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                            </form>
    </div>
@endsection

@section('modal')
    <div class="modal fade" id="modalCategory" tabindex="-1" aria-labelledby="modalCategoryLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalCategoryLabel">New Category</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="/question" method="POST">
                                    @csrf
                                    <div class="card-body">
                                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name', '') }}" placeholder="Add new category">
                                            @error('name')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                    </div>
                                    <div class="card-footer bg-transparent border-top-0">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                            </form>
                            </div>
                        </div>
                    </div>
@endsection