@extends('layout.master')

@section('title')
    - Detail
@endsection

@section('content')
    <div class="container">
        <div class="mb-3 p-5">
            @if (session('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{session('success')}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card-body">
                <div class="d-flex bd-highlight">
                    <div class="flex-grow-1 bd-highlight">
                        <h1>{{$questions->title}}</h1>
                    </div>
                        @if (Auth::id() === $questions->users->id)
                        <div class="p-2 bd-highlight">
                            <button class="btn btn-outline-success btn-sm my-2 my-sm-0" data-toggle="modal" data-target="#modalForum" type="submit"><i class="fa-solid fa-pen-to-square"></i></button>
                        </div>
                        <div class="p-2 bd-highlight">
                            <form action="/question/{{$questions->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" value="Delete" class="btn btn-danger btn-sm"><i class="fa-solid fa-trash"></i></button>
                            </form>
                        </div>
                        @endif
                </div>
                <span class="badge badge-primary badge-sm">
                    {{$questions->category->name}}
                </span>
                <p class="card-text">{{$questions->question}}</p>
                <p class="card-text">
                    <small class="text-muted">
                        {{$questions->users->name}} - {{$questions->users->created_at}}
                    </small></p>
            </div>
            <div class="image-scroll">
                <img src="{{asset('images/questions/'.$questions->image)}}" class="card-img-top" alt="IMAGE">
            </div>
            <br>

            @forelse ($questions->replies as $item)
            <div class="card">
                <h5 class ="card-header">{{$item->users->name}}</h5>
                <div class="card-body">
                  <p class="card-text">{{$item->reply}}</p>
                </div>
              </div>
            @empty
                <h4> No Answer </h4>
            @endforelse

            <hr>
            <form action="/reply" method="POST" class="mb-3">
                @csrf
                <input type="hidden" value="{{$questions->id}}" name="questions_id">
                <div class="form-group">
                  <label for="comment">Reply to this post</label>
                  <textarea
                    class="form-control"
                    name="reply"
                    rows="5"
                    required
                  ></textarea>
                  <button type="submit" class="btn btn-primary mt-2 mb-lg-5">
                    Submit reply
                  </button>
                </div>
            </form>
        </div>
    </div>

    @endsection

@section('modal')
    <div class="modal fade" id="modalForum" tabindex="-1" aria-labelledby="modalForumLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="modalForumLabel">Edit</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <form action="/question/{{$questions->id}}" method="POST" enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="card-body">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="title" name="title" value="{{ old('title', $questions->title) }}" placeholder="Title">
                                            @error('title')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                            <select name="category_id" class="form-control">
                                                <option value="">--Select Category--</option>
                                                @foreach ($categories as $category)
                                                    @if ($category->id === $questions->category_id)
                                                        <option value="{{$category->id}}" selected>{{$category->name}}</option>
                                                    @else
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <textarea class="form-control" id="question" name="question" cols="30" rows="10" placeholder="Type your question here...">{{ old('question', $questions->question) }}</textarea>
                                            @error('question')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="file" class="form-control" id="image" name="image">
                                            @error('image')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        {{-- <div class="form-group">
                                            <label>Category</label>
                                            <select name="category_id" class="form-control">
                                                <option value="">--Pilih Category--</option>
                                                @foreach ($categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id')
                                                <div class="alert alert-danger">{{ $message }}</div>
                                            @enderror
                                        </div> --}}
                                    </div>
                                    <div class="card-footer bg-transparent border-top-0">
                                        <button type="submit" class="btn btn-primary">Edit</button>
                                    </div>
                            </form>
                            </div>
                        </div>
                    </div>
@endsection
