<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reply extends Model
{
    protected $table = "replies";
    protected $fillable = ['reply', 'rating', 'questions_id', 'user_id'];


    // public function rating()
    // {
    // return $this->hasMany(Rating::class);
    // }

    // public function user()
    // {
    // 	return $this->belongsToMany('App\User');
    // }

    // public function questions()
    // {
    // 	return $this->belongsToMany('App\Question');
    // }


    public function users()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }

    public function questions()
    {
    	return $this->belongsTo('App\Question', 'questions_id');
    }

}

