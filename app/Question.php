<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = "questions";

    protected $guarded = [];

    public function users()
    {
    return $this->belongsTo('App\User', 'user_id');
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    // public function reply() {
    //     return $this->belongsToMany('App\Reply');
    // }

    public function replies() {
        return $this->hasMany('App\Reply', 'questions_id');
    }
}
