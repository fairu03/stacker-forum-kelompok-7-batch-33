<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;


class Profile extends Model
{
    protected $table = "profiles";
    protected $fillable = ['age', 'address', 'image', 'user_id'];

    public function user()
    {
    	return $this->belongsTo('App\User', 'user_id');
    }
}
