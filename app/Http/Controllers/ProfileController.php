<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Profile;
use File;
use App\User;
Use Alert;


class ProfileController extends Controller
{
    public function index()
    {
        $id = Auth::id();
        $profiles = Profile::where('user_id', $id)->first();
        // dd($profiles);
        return view('client.update', compact('profiles'));
    }



    public function update(Request $request, $id)
        {
            $request->validate([
                "name"=> 'required',
                "email"=> 'required',
                "image"=> 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                "age"=> 'required',
                "address"=> 'required',
            ]);

            // $input = $request->all();
            // $profiles = Profile::find($id);
            // $profiles->fill($input)->save();

            $profiles = Profile::find($id);


                $imageName = time().'.'.$request->image->extension();

                $request->image->move(public_path('images/img_profile'), $imageName);

                $profiles->image = $imageName;

            $profiles->age = $request['age'];
            $profiles->address = $request['address'];
            $profiles->image = $imageName;
            $profiles->save();

            Alert::success('Success', 'Data Updated Successfully');

            return redirect('/profile')->with('success', 'Profile Updated Successfully');
            }
}
