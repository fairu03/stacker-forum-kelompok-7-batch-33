<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Category;
use App\User;
use File;
use Auth;
Use Alert;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');

    }

    public function index()
    {
        if(request('search')){
            $questions = Question::where('title', 'like', '%' . request('search') . '%')->paginate(4);
        }else{
            $questions = Question::paginate(4);
        }

        $user = new User;
        $users_online = $user->allOnline();

        $users = User::all();

        $categories = Category::all();
        return view('question.index', compact('questions', 'users_online', 'users', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('question/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validasi
        $request->validate([
            'title' => 'required',
            'question' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'category_id' => 'required'
        ]);

        $imageName = time().'.'.$request->image->extension();

        $request->image->move(public_path('images/questions'), $imageName);

        $questions = new Question;

        $questions->title = $request['title'];
        $questions->question = $request['question'];
        $questions->image = $imageName;
        $questions->category_id = $request['category_id'];
        $questions->user_id = auth()->id();
        $questions->user_id = Auth::id();
        $questions->save();

        Alert::success('Success', 'New Forum Created Successfully');

        return redirect('question')->with('success', 'New Forum Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $questions = Question::find($id);
        $categories = Category::all();
        $users = User::find($id);
        $users = User::all();

        return view('question/show', compact('questions', 'categories', 'users'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Validasi
        $request->validate([
            'title' => 'required',
            'question' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        $questions = Question::find($id);
        $users = User::find($id);

        if ($request->has('image')){
            $path = 'images/questions/';
            File::delete($path . $questions->image);
            $imageName = time().'.'.$request->image->extension();

            $request->image->move(public_path('images/questions'), $imageName);

            $questions->image = $imageName;
        }

        $questions->title = $request['title'];
        $questions->question = $request['question'];
        $questions->category_id = $request['category_id'];
        $questions->user_id = auth()->id();
        $questions->save();

        Alert::success('Success', 'Forum Edited Successfully');

        return redirect(route('question.show',$id))->with('success', 'Forum Edited Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $questions = Question::findorfail($id);
        $questions->delete();

        $path = 'images/questions/';
        File::delete($path . $questions->image);

        Alert::success('Success', 'Forum Removed Successfully');

        return redirect('question')->with('success','Forum Removed Successfully');
    }
}
