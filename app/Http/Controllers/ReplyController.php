<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reply;
use App\Question;
use Auth;

class ReplyController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'reply' => 'required'
            // 'rating' => 'required',

        ]);

        $replies = new Reply;

        $replies->reply = $request->reply;
        $replies->questions_id = $request->questions_id;
        $replies->user_id = Auth::id();
        $replies->save();

        return redirect('/question/'.$request->questions_id);
    }
}
