<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Category;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(request('search')){
            $questions = Question::where('title', 'like', '%' . request('search') . '%')->paginate(4);
        }else{
            $questions = Question::paginate(4);
        }

        $user = new User;
        $users_online = $user->allOnline();

        $users = User::all();

        $categories = Category::all();
        return view('question.index', compact('questions', 'users_online', 'users', 'categories'));
        }
}
