<?php

use App\Category;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/master', function () {
    return view('layout.master');
});

Route::get('/', 'HomeController@index');


Route::resource('question', 'QuestionController');
Route::resource('category', 'CategoryController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// ini route ProfileController
Route::resource('profile', 'ProfileController')->only([
    'index', 'update'
]);

//Route Category
Route::post('/category', 'CategoryController@store');


//Route Reply
Route::post('/reply', 'ReplyController@store');


//Route Category
// Route::post('/profile', 'ProfileController@update');


// Route::get('/profile/{id}', 'ProfileController@index');
// Route::post('/profile', 'ProfileController@store');
// Route::post('/profile/{id}', 'ProfileController@update');
